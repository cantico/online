<?php
require_once dirname(__FILE__) . '/functions.php';

function onl_config()
{
    class temp
        {
        function temp()
            {
            $this->t_view_section = onl_translate('View section');
            $this->t_view_user_link = onl_translate('View link in user section');
            $this->t_view_ip = onl_translate('Display ip addresses');
            $this->t_all_view_ip = onl_translate('View addresses');
            $this->t_part_view_ip = onl_translate('View in part of address');
            $this->t_no_view_ip = onl_translate('No view addresses');
            $this->t_nb_users_section = onl_translate('Number of users in section, ordered by last hit date');
            $this->t_hide_anonymous_users = onl_translate('Hide annonymous users');

            $this->t_record = onl_translate('Record');

            $this->arr = $GLOBALS['ONL_CONFIG'];
            }

        }
    $tp = new temp();
    $addon = bab_getAddonInfosInstance('online');
    $GLOBALS['babBody']->addStyleSheet($addon->getStylePath().'online.css');
    $GLOBALS['babBody']->babecho(bab_printTemplate($tp, $addon->getTemplatePath()."main.html", "admin"));
}



function onl_config_record()
{

    $view_section = isset($_POST['view_section']) ? 1 : 0;
    $view_user_link = isset($_POST['view_user_link']) ? 1 : 0;
    $view_ip = isset($_POST['view_ip']) ? $_POST['view_ip'] : 0;
    $all_view_ip = isset($_POST['all_view_ip']) ? 1 : 0;
    $part_view_ip = isset($_POST['part_view_ip']) ? 1 : 0;
    $no_view_ip = isset($_POST['no_view_ip']) ? 1 : 0;
    $hide_anonymous = isset($_POST['hide_anonymous']) ? 1 : 0;
    $nb_users_section = is_numeric($_POST['nb_users_section']) ? $_POST['nb_users_section'] : 0;

    $GLOBALS['ONL_CONFIG'] = array(
                    'view_section' => $view_section,
                    'view_user_link' => $view_user_link,
                    'view_ip' => $view_ip,
                    'nb_users_section' => $nb_users_section,
                    'hide_anonymous_users' => $hide_anonymous
                );


    $db = &$GLOBALS['babDB'];
    $res = $db->db_query("SELECT * FROM onl_config");
    if ($res && $db->db_num_rows($res) > 0)
    {
        $db->db_query("UPDATE onl_config
                        SET view_section='".$view_section."',
                            view_user_link ='".$view_user_link."',
                            view_ip ='".$view_ip."',
                            nb_users_section ='".$nb_users_section."',
                            hide_anonymous_users ='".$hide_anonymous."'"
            );
    }
    else
    {
        $db->db_query("INSERT INTO onl_config (
                          view_section ,
                          view_user_link ,
                          view_ip ,
                          nb_users_section,
                          hide_anonymous_users )
                       VALUES (
                          '".$view_section."',
                          '".$view_user_link."',
                          '".$view_ip."',
                          '".$nb_users_section."',
                          '".$hide_anonymous."') "
           );
    }
}

// main


if (isset($_POST['action']) && $_POST['action'] == 'config')
{
    onl_config_record();
}

onl_config();

?>