<?php
include_once $GLOBALS['babInstallPath']."utilit/devtools.php";


function onl_translate($str)
{
    return bab_translate($str, 'online');
}

if (bab_isAddonInstalled('online'))
{
    $GLOBALS['ONL_CONFIG'] = $GLOBALS['babDB']->db_fetch_assoc($GLOBALS['babDB']->db_query("SELECT * FROM onl_config"));
    if (empty($GLOBALS['ONL_CONFIG']))
        {
        $GLOBALS['ONL_CONFIG'] = array(
                    'view_section' => 1,
                    'view_user_link' => 1,
                    'view_ip' => 0,
                    'nb_users_section' => 10,
                    'hide_anonymous_users' => 0
                );
        }
}

?>