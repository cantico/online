<?php
require_once dirname(__FILE__) . '/functions.php';

function online_ovml($args)
    {
    $sessions = bab_getActiveSessions();

    $korder = isset($args['order']) && in_array($args['order'], array('user_name','login_date','last_hit_date')) ? $args['order'] : 'user_name';
    $type = isset($args['type']) ? $args['type'] : 'all';


    $sessions_by_users = array();
    $lasthit_by_users = array();

    foreach($sessions as $arr)
    {
        if ($GLOBALS['ONL_CONFIG']['hide_anonymous_users'] && empty($arr['id_user'])){
            continue;
        }
        if (!empty($arr['id_user']))
            {
            if (!isset($sessions_by_users[$arr['id_user']]))
                {
                $sessions_by_users[$arr['id_user']] = 1;
                $lasthit_by_users[$arr['id_user']] = array($arr['last_hit_date'],$arr['session_id']) ;
                $order[$arr['session_id']] = $arr[$korder];
                }
            else
                {
                $sessions_by_users[$arr['id_user']]++;
                if ($lasthit_by_users[$arr['id_user']][0] < $arr['last_hit_date'])
                    {
                    unset($order[$lasthit_by_users[$arr['id_user']][1]]);
                    $lasthit_by_users[$arr['id_user']] = array($arr['last_hit_date'],$arr['session_id']);
                    $order[$arr['session_id']] = $arr[$korder];
                    }
                }
            }
        else
            {
            $arr['user_name'] = onl_translate('Anonymous user');
            $order[$arr['session_id']] = $arr[$korder];
            }



        $users[$arr['session_id']] = $arr;
    }

    natcasesort($order);

    $return = array();

    foreach($order as $session_id => $val)
        {
        if ('all' == $type || ('registered' == $type && !empty($u['id_user'])) || ('anonymous' == $type && empty($u['id_user'])))
            {
            $line = array();
            $u = &$users[$session_id];
            $line['Logged'] = empty($u['id_user']) ? 0 : 1;
            if (!empty($u['id_user'])) {
                $line['UserId'] = $u['id_user'];
                $line['DirectoryLink'] = bab_getUserDirEntryLink($u['id_user']);
            } else {
                $line['UserId'] = '';
                $line['DirectoryLink'] = '';
            }
            $line['UserName'] = $u['user_name'];
            $line['UserEmail'] = $u['user_email'];
            $line['PublicIp'] = $u['remote_addr'];
            $line['PrivateIp'] = $u['forwarded_for'];
            if ($GLOBALS['ONL_CONFIG']['view_ip'] == 1) {
                $line['PublicIp'] = preg_replace("/.[0-9]{1,3}[0-9]$/"," ",$line['PublicIp']);
                $line['PrivateIp'] = preg_replace("/.[0-9]{1,3}[0-9]$/"," ",$line['PrivateIp']);
            } elseif ($GLOBALS['ONL_CONFIG']['view_ip'] == 2) {
                $line['PublicIp'] = preg_replace("/([0-9]{1,3})/","---",$line['PublicIp']);
                $line['PrivateIp'] = preg_replace("/([0-9]{1,3})/","---",$line['PrivateIp']);
            } else {
                $line['PublicIp'] = preg_replace("/.[0-9]{1,3}$/",".---",$line['PublicIp']);
                $line['PrivateIp'] = preg_replace("/.[0-9]{1,3}$/",".---",$line['PrivateIp']);
            }
            $line['LoginDate'] = $u['login_date'];
            $line['LastHitDate'] = $u['last_hit_date'];
            $line['Sessions'] = isset($sessions_by_users[$u['id_user']]) ? $sessions_by_users[$u['id_user']] : 1;

            $return[] = $line;
            }
        }

    return $return;
    }



function online_translate($str)
{
    return onl_translate($str);
}


?>