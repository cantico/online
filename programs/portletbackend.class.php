<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
global $babInstallPath;
include_once $babInstallPath.'admin/acl.php';
require_once dirname(__FILE__).'/functions.php';

bab_Functionality::includefile('PortletBackend');

/**
 * 
 */
class Func_PortletBackend_online extends Func_PortletBackend
{
	public function getDescription()
	{
		return onl_translate("Display the number of online users");
	}
	
	public function select($category = null)
	{
		
		return array('online' => $this->Portlet_online());
	}
	
	
	
	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
		return $this->Portlet_online();
	}


	public function Portlet_online()
	{
		return new PortletDefinition_online();
	}
	
	/**
	 * return a list of action to use for configuration
	 * when fired, a parameter named backurl will be added to action, this parameter will contain an url to get back to the main configuration page
	 * each action can contain an icon and a title
	 * 
	 * @return multitype:Widget_Action
	 */
	public function getConfigurationActions()
	{
		
		return array();
	}
	
	/**
	 * get a list of categories supported by the backend
	 * @return Array 
	 */
	public function getCategories()
	{
		return array();
	}
}


/////////////////////////////////////////


class PortletDefinition_online implements portlet_PortletDefinitionInterface
{
	
	private $id;
	private $name;
	private $description;
	public  $addon;
	
	public function __construct()
	{
		$this->id 			= 'online';
		$this->name 		= 'Online';
		$this->description 	= 'Display the number of online users';
		$this->addon 		= bab_getAddonInfosInstance('online');
	}

	public function getId()
	{
		return $this->id; 
	}
	
	
	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;	
	}


	public function getPortlet()
	{		
		return new Portlet_online($this->id, $this->name, $this->description);
	}

	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}
	
	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}
	
	public function getConfigurationActions()
	{
		return array();
	}
	
	public function getPreferenceFields()
	{
		return array();
	}
}




class Portlet_online extends Widget_Item implements portlet_PortletInterface
{
	private $id;
	private $name;
	private $description;
	private $options = array();
	
	/**
	 * Instanciates the widget factory.
	 *
	 * @return Func_Widgets
	 */
	function Widgets()
	{
		return bab_Functionality::get('Widgets');
	}
		
		

	
	/**
	 */
	public function __construct($id, $name, $description)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		
		
	}

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/addonsincl.php';
		$addons = bab_addonsInfos::getRows();
			
		foreach ($addons as $value) {
		
			if ($value['title'] == 'online') {
				if(!bab_isAddonAccessValid($value['id'])){
					return '';
				}
			}
		}
		
		$sessions = bab_getActiveSessions();

		$users = array();
		$order = array();
		$anonymous = 0;
		foreach($sessions as $arr)
		{
			if ((isset($order[$arr['id_user']]) && $order[$arr['id_user']] < $arr['last_hit_date']) || !isset($order[$arr['id_user']]))
			{
				$order[$arr['id_user']] = $arr['last_hit_date'];
			}
	
			if ($arr['id_user'] > 0)
			{
				$users[$arr['id_user']] = array($arr['user_name'], $arr['user_email']);
			} else {
				$anonymous++;
			} 
		}
	
		$nb_user = count($users);
		$total = $nb_user;
		if (!$GLOBALS['ONL_CONFIG']['hide_anonymous_users']){
			$total+=$anonymous;
		}
	
		arsort($order);
	
		if ($total <= 1)
		{
			$title = $total.' '. onl_translate('user online');
		} else {
			$title = $total.' '. onl_translate('users online');
		}
	
		$str = '';
		if ($nb_user == 1)
		{
			$str = onl_translate('one registered user');
		} elseif ($nb_user > 1) {
			$str = count($users).' '.onl_translate('registered users');
		}
	
		if (!empty($str) && $anonymous > 0)
		{
			$str .= ' '.onl_translate('and').' ';
		}
	
		if ($anonymous == 1)
		{
			$str .= onl_translate('one anonymous user');
		} elseif($anonymous > 1) {
			$str .= $anonymous.' '.onl_translate('anonymous users');
		}

		
		$html = '
		<style scoped>
			#bab_online_portlet li:hover{
				background-color: #666;
				color: white;
			}
			#bab_online_portlet li:hover a{
				color: white !important;
			}
		</style>
		<div id="bab_online_portlet" class="bab_online" style="padding: 0.25em;">
			<div>
				<span style="display: block; font-size: 9.25pt; color: #000; font-size: 1em; font-weight: bold; margin: 0; padding: 0">
					'.$title.'
				</span>
			</div> 
			<div>';
		if (!$GLOBALS['ONL_CONFIG']['hide_anonymous_users']){
			$html.= '<h5 style="display: block; font-size: 85%; opacity: 0.7; padding: 0 0.2em; vertical-align: middle;">'.$str.'</h5>';
		}
		$html.= '<ul style="margin: 0.3em 0 0; padding-left: 25px;">';
	
		$max = 0;
		$inserted_users = array();
		foreach($order as $id_user => $last_hit)
		{
			
			if ($max >= $GLOBALS['ONL_CONFIG']['nb_users_section'])
			{
				break;
			}
	
			if (isset($users[$id_user]) && !isset($inserted_users[$id_user]))
			{
				$inserted_users[$id_user] = 1;
				$delay = time() - $last_hit;
				$html.= '<li style="color: #777;">
							<a style="color: #777; display: block; font-size: 9pt; text-decoration: none;" title="'.onl_translate('Last hit').' : '.$delay.' '.onl_translate('seconds ago').'" href="mailto:'.$users[$id_user][1].'">
								'.$users[$id_user][0].'
							</a>
						</li>';
				$max++;
			}
		}
	
		
		$html.= '	<li style="color: #777";><a style="color: #777; display: block; font-size: 9pt; text-decoration: none;" href="'.$GLOBALS['babUrlScript'].'?tg=oml&file=addons/online/main.html">'.onl_translate('View all').'</a></li>
				</ul>
			</div>
		</div>';
		
		return $html;
	}

	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new PortletDefinition_online($this->id, $this->name, $this->description);
	}
	
	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}
	
	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}
	
	public function setPortletId($id)
	{
		
	}
}

