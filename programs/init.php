<?php
require_once dirname(__FILE__) . '/functions.php';


function online_getAdminSectionMenus(&$url, &$text)
{
	static $i=0;
	if(!$i && !empty($GLOBALS['BAB_SESS_USERID']))
	{
		$url = $GLOBALS['babAddonUrl']."admin";
		$text = bab_translate('Online', $GLOBALS['babAddonFolder']);
		$i++;
		return true;
	}
}

function online_getUserSectionMenus(&$url, &$text)
{
	static $nbMenus=0;
	if( !$nbMenus && !empty($GLOBALS['ONL_CONFIG']['view_user_link']))
	{
		$url = $GLOBALS['babUrlScript']."?tg=oml&file=addons/online/main.html";
		$text = bab_translate('Online', $GLOBALS['babAddonFolder']);
		$nbMenus++;
		return true;
	}
	return false;
}

function online_onSectionCreate( &$title, &$content)
{
	if (!$GLOBALS['ONL_CONFIG']['view_section'])
		return false;

	$sessions = bab_getActiveSessions();

	$users = array();
	$order = array();
	$anonymous = 0;
	foreach($sessions as $arr)
	{
		if ((isset($order[$arr['id_user']]) && $order[$arr['id_user']] < $arr['last_hit_date']) || !isset($order[$arr['id_user']]))
			$order[$arr['id_user']] = $arr['last_hit_date'];

		if ($arr['id_user'] > 0)
			$users[$arr['id_user']] = array($arr['user_name'], $arr['user_email']);
		else
			$anonymous++; 
	}

	$nb_user = count($users);
	$total = $nb_user+$anonymous;

	arsort($order);

	if ($total <= 1)
		$title = $total.' '. onl_translate('user online');
	else
		$title = $total.' '. onl_translate('users online');

	$section = new bab_myAddonSection();
	$header = $section->addElement('text');
	$str = '';
	if ($nb_user == 1)
		{
		$str = onl_translate('one registered user');
		}
	elseif ($nb_user > 1)
		{
		$str = count($users).' '.onl_translate('registered users');
		}

	if (!empty($str) && $anonymous > 0)
		{
		$str .= ' '.onl_translate('and').' ';
		}

	if ( $anonymous == 1)
		{
		$str .= onl_translate('one anonymous user');
		}
	elseif($anonymous > 1)
		{
		$str .= $anonymous.' '.onl_translate('anonymous users');
		}
	$section->pushHtmlData($header,$str);

	$list = $section->addElement('list');
	$max = 0;
	$inserted_users = array();
	foreach($order as $id_user => $last_hit)
		{
		
		if ($max >= $GLOBALS['ONL_CONFIG']['nb_users_section'])
			{
			break;
			}

		if (isset($users[$id_user]) && !isset($inserted_users[$id_user]))
			{
			$inserted_users[$id_user] = 1;
			$delay = time() - $last_hit;
			$section->pushHtmlData($list, $users[$id_user][0], array('href' => 'mailto:'.$users[$id_user][1], 'title' => onl_translate('Last hit').' : '.$delay.' '.onl_translate('seconds ago')));
			$max++;
			}
		}

	$section->pushHtmlData($list, onl_translate('View all'), array('href' => $GLOBALS['babUrlScript']."?tg=oml&file=addons/online/main.html"));
	
	$content = $section->getHtml();

		
	return true;
}


function online_upgrade($version_base,$version_ini)
{
	@bab_functionality::includefile('PortletBackend');
	if (class_exists('Func_PortletBackend')) {
		$addonName = 'online';
		$addonInfo = bab_getAddonInfosInstance($addonName);
		
		$addonPhpPath = $addonInfo->getPhpPath();
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->registerClass('Func_PortletBackend_online', $addonPhpPath . 'portletbackend.class.php');
	}
	
	$tables = new bab_synchronizeSql(dirname(__FILE__) . '/dump.sql');
	if (isset($tables->return['onl_config']))
	{
		return true;
	}

	return false;
}

?>