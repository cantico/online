CREATE TABLE `onl_config` (
  `view_section` tinyint(3) unsigned NOT NULL default '1',
  `view_user_link` tinyint(3) unsigned NOT NULL default '1',
  `view_ip` tinyint(3) unsigned NOT NULL default '0',
  `nb_users_section` tinyint(3) unsigned NOT NULL default '0',
  `hide_anonymous_users` tinyint(3) unsigned NOT NULL default '0'
);